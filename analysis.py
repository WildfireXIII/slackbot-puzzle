import pandas as pd
import base64

df = pd.read_csv('data.csv', header=None)

print("Trigger\t\tEncoded Text")
print("---------------------------------")
for row_index in range(df.shape[0]):
    text = str(base64.b64decode(df[1][row_index]))[2:-1]

    print(df[0][row_index], "\t", text)
